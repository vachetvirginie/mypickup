<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="index.css">
    <title>Document</title>
</head>

<body>
    <div id="parallax-world-of-ugg">

        <section>
            <div class="title">
                <h1>MY PICKUP</h1>
                <h3>Livraison de colis entre particuliers</h3>
                <form class="center iq-md-7 iq-sm-9" id="username" action="login.php" method="POST">

                    <div>
<h5>Se connecter: <br/></h5>
                        <i class="fa fa-envelope icon" id="email" aria-hidden="true"></i>
                        <input class="input-form input" type="text" name="pseudo" placeholder="write your pseudo" required="required" />
                    </div>

                    <div>
                        <i class="fa fa-lock icon" id="pass" aria-hidden="true"></i>
                        <input class="input-form input" type="password" name="mdp" placeholder="write your password" required="required" />
                    </div>


                    <button> Se connecter </button>

                </form>
            </div>
        </section>

        <section>
            <div class="parallax-one">
                <h2>Envoyez vos colis, objets et meubles en toute sérénité et jusqu'à 80% moins cher, assurance incluse</h2>
            </div>
        </section>

        <section>
            <h1 style="text-align:center;"> Comment ça marche ?</h1>
            <div class="block">

                <div class="grid-unit">
                    <img style="width:350px;height:300px;" src="http://www.traildutrefle.com/images/inscri.png" />
                    <h4>Inscrivez vous en quelques secondes afin d'acceder aux annonces.<br/>Vous trouverez ci dessous le formulaire d'inscription.<br/> </h4>
                </div>
                <div class="grid-unit">
                    <img style="width:350px;height:300px;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTwnXJHgMAzhN86Dp-CliWBCqv7j9wFd2XFhfDbAwQ83CjIc9-6LQ" />
                    <h4>Connectez vous à votre espace afin d acceder aux annonces ou en publier.<br/> </h4>
                </div>
                <div class="grid-unit">
                    <img src="https://malignel.transilien.com/wp-content/uploads/2015/10/validation.png" />
                    <h4>Validez et c'est parti.<br/> </h4>
                </div>
<div class="price">

<h1>Nos tarifs     </h1>
<div class="promos">  
<div class="promo">
  <div class="deal">
    <span>Premium</span>
    
  </div>
  <span class="price">$35</span>
  <ul class="features">
    <li>Entre 1 et 60 km</li>
    <li>Pas plus de 10kg</li>
     
  </ul>
  
</div>
<div class="promo scale">
  <div class="deal">
    <span>Plus</span>
   
  </div>
  <span class="price">$150</span>
  <ul class="features">
    <li>Kilometrage illimité</li>
    <li>Jusqu'à 60 kg</li>
     
  </ul>

</div>
<div class="promo">
  <div class="deal">
    <span>Basic</span>
   
  </div>
  <span class="price">$80</span>
  <ul class="features">
    <li>Entre 60 et 120 km</li>
    <li>Jusqu'à 30kg</li>
      
  </ul>
 

</div>
</div>
</div>

</div>
        </section>

        <section>
            <div class="parallax-two">
                <h2>INSCRIPTION</h2>
                <form form id="inscr" action="inscription.php" method="POST">
                    <div class="user">
                        <i class="fa fa-user icon" id="email" aria-hidden="true"></i>
                        <input class="input-form full-width" type="text" placeholder="Your pseudo" name="pseudo" required="required" />
                    </div>





                    <div>
                        <i class="fa fa-lock icon" id="email" aria-hidden="true"></i>
                        <input class="input-form full-width" type="password" placeholder="write password" name="mdp" required="required" />

                    </div>




                    <button>S'inscrire</button>


                </form>
            </div>
        </section>

        <section>
            <div class="block">

            </div>
        </section>




</body>

</html>