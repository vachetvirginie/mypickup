<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="card.css">
    <title>Document</title>
</head>
<script>
    $('form').card({
        container: '.card-wrapper',
        width: 280,

        formSelectors: {
            nameInput: 'input[name="first-name"], input[name="last-name"]'
        }
    });
</script>

<body>

    <form>
        <div class="form-container">
            <div class="personal-information">
                <h1>Payment Information</h1>
            </div>
            <!-- end of personal-information -->

            <input id="input-field" type="text" name="streetaddress" required="required" autocomplete="on" maxlength="45" placeholder="Streed Address" />
            <input id="column-left" type="text" name="city" required="required" autocomplete="on" maxlength="20" placeholder="City" />
            <input id="column-right" type="text" name="zipcode" required="required" autocomplete="on" pattern="[0-9]*" maxlength="5" placeholder="ZIP code" />
            <input id="input-field" type="email" name="email" required="required" autocomplete="on" maxlength="40" placeholder="Email" />

            <div class="card-wrapper"></div>
            <input id="column-left" type="text" name="first-name" placeholder="First Name" />
            <input id="column-right" type="text" name="last-name" placeholder="Surname" />
            <input id="input-field" type="text" name="number" placeholder="Card Number" />
            <input id="column-left" type="text" name="expiry" placeholder="MM / YY" />
            <input id="column-right" type="text" name="cvc" placeholder="CCV" />
            <input id="input-button" type="submit" value="Submit" />
    </form>
    </div>
</body>

</html>